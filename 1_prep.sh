#!/bin/bash
export PYTHONPATH=codes/dynibatch:codes/tucmi_tf:codes/prep
#paths
exp="experiment/Exp_0(template)"
listes=$exp"/Out/listes/"
dico=$exp"/Out/dictionary/"
f2l=$exp"/Out/label_file2label/"
config=$exp"/Out/config/"
datasplit=$exp"/Out/datasplit/"
datasets=$exp"/datasets/"

#create liste of all files
ls ${datasets}**/*.wav |cut -f 5 -d '/' > ${listes}liste_all.csv

#liste of  all species code
cut -f 1 -d '_' ${listes}liste_all.csv | sort -u > ${listes}liste_codes.csv

#create liste of separated train and test files
ls ${datasets}TrainSet/*.wav |cut -f 5 -d '/' > ${listes}liste_train.csv
ls ${datasets}TestSet/*.wav |cut -f 5 -d '/' > ${listes}liste_test.csv

#create label_file.txt
>${f2l}label_file.txt
l=1
for i in $(cat ${listes}liste_codes.csv); do
  printf "$l,$i\n" >> ${f2l}label_file.txt
  l=$(($l + 1))
done

#create the dico  code2label.json
python codes/prep/dico.py ${f2l}label_file.txt ${dico}code2label.json

#create the file2label.csv
python codes/prep/file2lab.py ${dico}code2label.json ${listes}liste_all.csv ${f2l}file2label_all.csv ${listes}liste_train.csv ${f2l}file2label_train.csv

#create/recreate configtrain.jl and test?


