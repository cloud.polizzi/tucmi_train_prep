#create file2label file with the dictionary and the total liste of files to create the file to label file
import json
import pandas as pd
import argparse
if __name__ == "__main__":
    # parse parameters
    parser = argparse.ArgumentParser()
    parser.add_argument("json_path", type=str,
                        help="code2label.json dictionary path.")

    parser.add_argument("liste_all_path", type=str,
                        help="liste_all.csv file path")
    parser.add_argument("f2l_all_path", type=str,
                        help="file2label_all.csv file path")

    parser.add_argument("liste_train_path", type=str,
                        help="liste_train.csv file path")
    parser.add_argument("f2l_train_path", type=str,
                        help="file2label_train.csv file path")

    args = parser.parse_args()


    with open(args.json_path) as f:
        dico = json.load(f)
    liste=pd.read_csv(args.liste_all_path,header=None)
    liste[1]=0
    for i in liste.index:
        code=liste.loc[i]
        liste.loc[i,(1)]=dico[code.values[0][0:4]]
    #save to csv
    liste.to_csv(args.f2l_all_path,header=None, sep=',',index=None)
    #
    liste=pd.read_csv(args.liste_train_path,header=None)
    liste[1]=0
    for i in liste.index:
        code=liste.loc[i]
        liste.loc[i,(1)]=dico[code.values[0][0:4]]
    #save to csv
    liste.to_csv(args.f2l_train_path,header=None, sep=',',index=None)
