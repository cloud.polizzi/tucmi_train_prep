# for this code you need the file .txt with [label,code] and the file .csv with [file name]
import json
import pandas as pd
import argparse
from dynibatch.utils.segment_container import create_segment_containers_from_audio_files
from dynibatch.parsers.label_parsers import CSVFileLabelParser
from dynibatch.utils import datasplit_utils

if __name__ == "__main__":
    # parse parameters
    parser = argparse.ArgumentParser()
    parser.add_argument("audio_path", type=str,
                        help="Audio-tain directory path")
    parser.add_argument("f2l_train", type=str,
                        help="file2label_train.csv file path")
    parser.add_argument("label_file_train", type=str,
                        help="label_file.txt file path")
    parser.add_argument("split_path", type=str,
                        help="datasplit.jl out path")

    args = parser.parse_args()

 

    sc_gen= create_segment_containers_from_audio_files(args.audio_path) # là où sont les WAV

    # les arguments du parser vont aller chercher (file_to_labels, label_file.txt) (donc à changer les paths en fonction de ton expérience)

    parser=CSVFileLabelParser(args.f2l_train,label_file=args.label_file_train

    # attention au indentation (ne pas copier coller)
    sc_list=[]
    for sc in sc_gen:
        try:
            sc.labels=parser.get_label(sc.audio_path)
            sc_list.append(sc)
        except KeyError:
            pass


    datasplit_utils.write_datasplit(dico,args.split_path,compress=0)

    #B) Si tu veux que ça se fAlse automatiquement:
    print "Quel ratio pour le training set? (between 0 and 1)"
    print "(validation and test set ratio are extrapolated)"
    tr=input()
    v=='n'
    while (tr<=0) | (tr>=1) | (v!="y") | (v!="yes") | (v="Yes") | (v="YES"):
        if tr==0:
            print "With 0, you have nothing to train your model, try again..."
            tr=input()
        elif tr==1:
            print "WARNING: with a ratio of 1, nothing will be left for the test and validation set."
            print "Are you sure you want to continue with 1 ?"
            v=raw_input("( y to continue ): ")
        elif (tr<0) | (tr>1):
            print "Out of bound, try again... (between 0 and 1)"
            tr=input()




    dico=datasplit_utils.create_random_datasplit(sc_list,train_ratio=tr,validation_ratio=(1-tr)/2,test_ratio=(1-tr)/2)

    #sauver le dico :
    datasplit_utils.write_datasplit(dico,args.split_path,compress=0)
