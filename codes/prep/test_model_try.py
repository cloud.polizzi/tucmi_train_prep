import argparse
import json
#from keras.models import model_from_json
from keras.models import load_model
from tucmi.data_provider import TUCMIDataGen

from keras import backend as K; 
import tensorflow as tf; 
config = tf.ConfigProto(); config.gpu_options.allow_growth = True; sess = tf.Session(config=config); K.set_session(sess);

import logging
logging.getLogger().setLevel(logging.ERROR)

from collections import defaultdict
import numpy as np


def parse_bird35_targets(path_grountruth):
    """
    Parses test labels of the original dataset, written as

        <filename>,<class_id>,<class_code>,<1 if class is in filename, 0 otherwise>,<PublicTest/PrivateTest>
    ...

    Returns a dictionary with the following mapping:
        <filename>:<set of class_ids>
    """

    data = np.genfromtxt(path_grountruth, delimiter=",", dtype=None, usecols=(0,1,3),encoding='UTF-8')
    targets = defaultdict(set)



    for d in data:
        if d[2] == 1:
            targets[d[0]].add(int(d[1]))

    return targets


def process(config_path_train, config_path_test,
            model_path, setname='default', batch_size=1, with_targets=False):

    # Create tucmi data generator with training config,
    # just to get the class ids
    class_ids = TUCMIDataGen(config_path_train, is_training=True, batch_size=batch_size).class_ids

    # Load model
    model = load_model(model_path)

    # Create test data generator
    data_gen = TUCMIDataGen(config_path_test, is_training=False, batch_size=batch_size)

    # Predict
    predictions = []
    filenamesf = []
    for features, filenames in data_gen.generate_once(setname, with_targets=with_targets):
        predictions.append(model.predict_on_batch(features))
        filenamesf.append(filenames)

    return predictions, filenamesf


if __name__ == "__main__":
    # parse parameters
    parser = argparse.ArgumentParser()
    parser.add_argument("config_path_train", type=str,
                        help="Dynibatch training config path.")
    parser.add_argument("config_path_test", type=str,
                        help="Dynibatch test config path.")
    parser.add_argument("model_path", type=str,
                        help="Path of the trained Keras model.")
    parser.add_argument("output_path", type=str,
                        help="Path of the output csv prediction file.")
    args = parser.parse_args()

    predictions, filenamesf = process(args.config_path_train, args.config_path_test, args.model_path)

    # iCI FAIRE LA PROBA/ FICHIER

    filenamesf = np.squeeze(filenamesf)
    predictions = np.squeeze(predictions)

    per_file = {}
    for fn in np.unique(filenamesf):
            per_file[fn] = predictions[filenamesf == fn, :].max(axis=0)

        # on va créer une liste, pour après l'exporter en csv
    csv="\n".join([f + ',' + ",".join([str(x) for x in v]) for f, v in per_file.items()])
        # ecrire le csv avec le output_path qu'on a mis en argument
    open(args.output_path, 'w').write(csv)









#    # write predictions
#    outfile = open(args.output_path, "w")


    # write predictions
#    outfile = open(args.output_path, "w")
#    for f, p in zip(filenamesf, predictions):
#        outfile.write("{},{}\n".format(f[0], ",".join(map(str, p[0]))))
#    outfile.close()
