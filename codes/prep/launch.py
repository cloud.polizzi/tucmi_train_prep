# for this code you need the file .txt with [label,code] and the file .csv with [file name]
import json
import pandas as pd
import argparse
import pickle
#data memory managment
from keras import backend as K
import tensorflow as tf
config = tf.ConfigProto()
config.gpu_options.allow_growth = True
sess = tf.Session(config=config)
K.set_session(sess);

# disable annoying log
import logging
logging.getLogger().setLevel(logging.ERROR)

# run tucmi algorithm
from tucmi.data_provider import prepare_data
from tucmi import training_model


if __name__ == "__main__":
    # parse parameters
    parser = argparse.ArgumentParser()

    parser.add_argument("config_path_train", type=str,
                        help="config_train.js file path")
    parser.add_argument("output", type=str,
                        help="output directory")
    args = parser.parse_args()

    #Training:
	num_bird_segments, num_segments = prepare_data(args.config_path_train, args.output_dir,write_data=True, write_noise=False, do_show_stats=True);
	dataframe_path = output_dir + "/dataframe.h5";
	history = training_model.process(config_path_train, dataframe_path, output_dir, batch_size=64);
