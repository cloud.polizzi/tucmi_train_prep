source activate tucmi

##Ajouter le pythonPath de dynibatch et de tucmi

export PYTHONPATH=/home/hcube/codes/dynibatch:/home/hcube/codes/tucmi_tf 

##
ipython

####
####

from dynibatch.utils.segment_container import create_segment_containers_from_audio_files
from dynibatch.parsers.label_parsers import CSVFileLabelParser
from dynibatch.utils import datasplit_utils

sc_gen= create_segment_containers_from_audio_files("/home/hcube/data/exp9/TRAIN") # là où sont les WAV

# les arguments du parser vont aller chercher (file_to_labels, label_file.txt) (donc à changer les paths en fonction de ton expérience)

parser=CSVFileLabelParser("/home/hcube/experiences/exp9/TRAIN/file2label_train.csv",label_file="/home/hcube/experiences/exp9/TRAIN/label_file_anoures.txt")

# attention au indentation (ne pas copier coller) 
sc_list=[]
for sc in sc_gen:             
    try:
        sc.labels=parser.get_label(sc.audio_path)
        sc_list.append(sc)
    except KeyError:
        pass





#A)Si tu veux choisir TOI, ce qu’il y aura dans le train, valid et test alors: 

dico=datasplit_utils.create_random_datasplit(sc_gen,train_ratio=0.85,validation_ratio=0.10,test_ratio=0.05)
dico



train=open("/home/hcube/experiences/exp8/TRAIN/Listessplit/liste_train.csv","r")
line=train.readlines()
strip_list=[item.strip() for item in line]
dico['sets']['train'] = set(strip_list)  # à changer et mettre ‘test’ à la place de ‘train’ quand tu fera pour le test, et idem pour la validation


train=open("/home/hcube/experiences/exp8/TRAIN/Listessplit/liste_test_vic.csv","r")
line=train.readlines()
strip_list=[item.strip() for item in line]
dico['sets']['test'] = set(strip_list) 


train=open("/home/hcube/experiences/exp7/TRAIN/Listessplit/liste_validation.csv","r")
line=train.readlines()
strip_list=[item.strip() for item in line]
dico['sets']['validation'] = set(strip_list) 



#sauver le dico :

datasplit_utils.write_datasplit(dico,"/home/hcube/experiences/exp8/TRAIN",compress=0)


#B) Si tu veux que ça se fasse automatiquement: 

dico=datasplit_utils.create_random_datasplit(sc_list,train_ratio=0.76,validation_ratio=0.12,test_ratio=0.12)
#sauver le dico :

datasplit_utils.write_datasplit(dico,"/home/hcube/experiences/exp9/TRAIN",compress=0)







