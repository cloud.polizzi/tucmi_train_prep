# for this code you need the file .txt with [label,code] and the file .csv with [file name]
import json
import pandas as pd
import argparse
if __name__ == "__main__":
    # parse parameters
    parser = argparse.ArgumentParser()
    parser.add_argument("label_file_path", type=str,
                        help="label_file.txt file path")
    parser.add_argument("json_path", type=str,
                        help="code2label.json dictionary save path.")
    args = parser.parse_args()

    table={}
    with open(args.label_file_path) as f:
        for line in f:
            parts=line.rstrip().split(',')
            for code in parts[1:]:
                table[code] = parts[0]
    #save
    dic=json.dumps(table)
    f=open(args.json_path,"w")
    f.write(dic)
    f.close
