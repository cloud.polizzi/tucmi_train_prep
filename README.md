Tucmi_train_prep

To run the code:

1- Create tour Exp_## directory in the experiment directory by copying the template (Exp0).

2- Put your files in the directory : "experiment/Exp_##/datasets/TestSet" and ".../TrainSet" by spliting your main dataset by hand first (aumatisation code not ready yet).

3- Change the path in the 1_prep.sh code to match the experiement directory number (Exp_1, Exp_2, Exp_3 ...).

4- Launch the bash 1_prep.sh to create the dictionary, label\_file, file2label and other lists (in: experiment/Exp\_#/Out).

5- Next step has yet to be completed.
