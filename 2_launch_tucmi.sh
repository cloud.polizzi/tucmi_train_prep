#!/bin/bash
echo "Choose the GPU (0 or 1):"
read g
export CUDA_VISIBLE_DEVICES=$g
export PYTHONPATH=codes/dynibatch:codes/tucmi_tf:codes/prep

ex="experiences/T1/"
config="Configs/"
outmodel="res_models/"
inmodel="res_models/models/"
outtest="res_test"

python codes/prep/launch.py ${ex}${config}config_train.json ${ex}${outmodel}

#find the best(latest) model:

bestepo=$(ls ${ex}${inmodel} | sort -t= -nr -k3 | cut -f 5 -d '/' | cut -f 2 -d '.' | cut -f 1 -d '-'| head -1)

python codes/prep/test_model_try.py ${ex}${config}config_train.json ${ex}${config}config_test.json ${ex}${inmodel}model.${bestepo}- ${ex}${outtest}result_test.csv
